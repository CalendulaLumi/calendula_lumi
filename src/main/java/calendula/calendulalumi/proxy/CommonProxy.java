package calendula.calendulalumi.proxy;

import java.io.File;

import calendula.calendulalumi.networking.PacketHandler;
import calendula.calendulalumi.utils.Config;
import net.minecraft.item.Item;
import net.minecraftforge.common.config.Configuration;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;

@Mod.EventBusSubscriber
public class CommonProxy 
{
	public static Configuration config;
	
	public void preInit(FMLPreInitializationEvent event)
	{
		File directory = event.getModConfigurationDirectory();
		config = new Configuration(new File(directory.getPath(), "calenluna.cfg"));
		Config.readConfig();
		
		PacketHandler.registerMessages("calenluna");
	}
	
	public void registerItemRenderer(Item item, int meta, String id)
	{
		
	}
}