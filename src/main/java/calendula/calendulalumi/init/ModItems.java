package calendula.calendulalumi.init;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import calendula.calendulalumi.items.bases.ItemBase;
import calendula.calendulalumi.items.bases.tools.ToolPickaxe;
import net.minecraft.item.Item;
import net.minecraft.item.Item.ToolMaterial;
import net.minecraftforge.common.util.EnumHelper;

public class ModItems 
{
	public static final List<Item> ITEMS = new ArrayList<Item>();
	
	//Materials
	public static final ToolMaterial TOOL_STEEL = EnumHelper.addToolMaterial("tool_steel", 3, 754, 10.0F, 2.5F, 12);
	
	//Items
	public static final Item COPPER_INGOT = new ItemBase("copper_ingot");
	public static final Item STEEL_INGOT = new ItemBase("steel_ingot");
	
	//Tools
	public static final Item STEEL_PICKAXE = new ToolPickaxe("steel_pickaxe", TOOL_STEEL);
}
