package calendula.calendulalumi.init;

import java.util.ArrayList;
import java.util.List;

import calendula.calendulalumi.blocks.BlockBase;
import net.minecraft.block.Block;
import net.minecraft.block.material.Material;

public class ModBlocks 
{
	public static final List<Block> BLOCKS = new ArrayList<Block>();

	public static final Block COPPER_ORE = new BlockBase("copper_ore", Material.ROCK);
}
