package calendula.calendulalumi.utils;

import org.apache.logging.log4j.Level;

import calendula.calendulalumi.CalendulaLumi;
import calendula.calendulalumi.proxy.CommonProxy;
import net.minecraftforge.common.config.Configuration;

public class Config 
{
	private static final String CATEGORY_GENERAL = "general";
	
	//Mod-wide settings
	public static boolean modEnabled = true;
	public static double starPowerMultiplicator = 1.0;
	
	public static void readConfig()
	{
		Configuration cfg = CommonProxy.config;
		try
		{
			cfg.load();
			initGeneralConfig(cfg);
		}
		catch (Exception e1)
		{
			CalendulaLumi.logger.log(Level.ERROR, "Problem loading config file!", e1);
		}
		finally
		{
			if(cfg.hasChanged())
			{
				cfg.save();
			}
		}
	}
	
	private static void initGeneralConfig(Configuration cfg)
	{
		cfg.addCustomCategoryComment(CATEGORY_GENERAL, "Calenluna settings");
		modEnabled = cfg.getBoolean("modEnabled", CATEGORY_GENERAL, true, "Control availability of the mod");
		starPowerMultiplicator = cfg.getFloat("starPowerMultiplicator", CATEGORY_GENERAL, 1.0F, 0.0F, 50.0F, "Control starpower gathering rate");
	}
}
