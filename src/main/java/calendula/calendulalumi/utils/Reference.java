package calendula.calendulalumi.utils;

public class Reference 
{
	public static final String MOD_ID = "cl";
	public static final String NAME = "Calenluna";
	public static final String VERSION = "Particle 1";
	public static final String ACCEPTED_VERSIONS = "{1.12.2}";
	public static final String CLIENT_PROXY_CLASS = "calendula.calendulalumi.proxy.ClientProxy";
	public static final String COMMON_PROXY_CLASS = "calendula.calendulalumi.proxy.CommonProxy";
}
