package calendula.calendulalumi.utils;

public interface IHasModel 
{
	public void registerModels();
}
