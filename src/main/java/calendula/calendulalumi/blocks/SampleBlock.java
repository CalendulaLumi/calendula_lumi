package calendula.calendulalumi.blocks;

import net.minecraft.block.SoundType;
import net.minecraft.block.material.Material;

public class SampleBlock extends BlockBase
{
	public SampleBlock(String name, Material material) 
	{
		super(name, material);
		
		setSoundType(SoundType.ANVIL);
		setHardness(5.0F);
		setResistance(15.0F);
		setHarvestLevel("pickaxe", 2);
		setLightLevel(1.0F);
	}
}
