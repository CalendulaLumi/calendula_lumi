package calendula.calendulalumi.networking;

import io.netty.buffer.ByteBuf;
import net.minecraft.block.Block;
import net.minecraft.client.Minecraft;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.util.math.BlockPos;
import net.minecraft.util.math.RayTraceResult;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraft.world.World;
import net.minecraftforge.fml.common.FMLCommonHandler;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

public class PacketSendKey implements IMessage
{
	private BlockPos blockPos;
	
	@Override
	public void fromBytes(ByteBuf buf)
	{
		blockPos = BlockPos.fromLong(buf.readLong());
	}
	
	@Override
	public void toBytes(ByteBuf buf)
	{
		buf.writeLong(blockPos.toLong());
	}
	
	public PacketSendKey()
	{
		RayTraceResult result = Minecraft.getMinecraft().objectMouseOver;
		blockPos = result.getBlockPos();
	}
	
	public static class Handler implements IMessageHandler<PacketSendKey, IMessage>
	{
		@Override
		public IMessage onMessage(PacketSendKey message, MessageContext ctx)
		{
			FMLCommonHandler.instance().getWorldThread(ctx.netHandler).addScheduledTask(() -> handle(message, ctx));
			return null;
		}
		
		private void handle(PacketSendKey message, MessageContext ctx)
		{
			EntityPlayerMP playerEntity = ctx.getServerHandler().player;
			World world = playerEntity.getEntityWorld();
			
			if(world.isBlockLoaded(message.blockPos))
			{
				Block block = world.getBlockState(message.blockPos).getBlock();
				playerEntity.sendMessage(new TextComponentString(TextFormatting.GREEN + "Hit block: " + block.getRegistryName()));
			}
		}
	}
}
