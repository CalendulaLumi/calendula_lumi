package calendula.calendulalumi.items.bases.tools;

import calendula.calendulalumi.CalendulaLumi;
import calendula.calendulalumi.init.ModItems;
import calendula.calendulalumi.utils.IHasModel;
import net.minecraft.creativetab.CreativeTabs;
import net.minecraft.item.ItemPickaxe;

public class ToolPickaxe extends ItemPickaxe implements IHasModel
{
	public ToolPickaxe(String name, ToolMaterial material) 
	{
		super(material);
		setUnlocalizedName(name);
		setRegistryName(name);
		setCreativeTab(CreativeTabs.TOOLS);
		
		ModItems.ITEMS.add(this);
	}

	@Override
	public void registerModels() 
	{
		CalendulaLumi.proxy.registerItemRenderer(this, 0, "inventory");
	}
}
